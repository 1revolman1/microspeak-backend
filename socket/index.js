const groupBy = require("lodash/groupBy");
const express = require("express");
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http, {
  path: "/api/socket",
});
const { UserModel, ChatModel, MessageModel } = require("../db");
const chat = require("../db/chat");

let connectionCount = 0;
io.use(async function (socket, next) {
  const handshakeData = socket.request;
  console.log("middleware:", handshakeData._query["nick"]);
  const { _id } = await UserModel.findOneAndUpdate(
    {
      nickname: handshakeData._query["nick"],
    },
    { isOnline: true },
    { new: true }
  ).select({ _id: 1 });
  socket._id = _id;
  next();
});

io.on("connection", async (socket) => {
  connectionCount += 1;
  console.log("a user connected", connectionCount, socket._id);
  socket.on("connect-to-channel", async (chatID) => {
    //chatID - chat id
    await socket.join(chatID, async () => {
      const messages = await MessageModel.find({
        chat: chatID,
      });
      const UsersInChat = await ChatModel.findOne({ _id: chatID }).select({
        users: 1,
      });
      const infoAboutChatUsers = await UserModel.find({
        _id: { $in: UsersInChat.users },
      }).select({ nickname: 1, isOnline: 1, avatar: 1 });
      const newMessages = messages.map(({ _doc }) => {
        return {
          ..._doc,
        };
      });
      socket.emit("initial chat data", [
        newMessages,
        groupBy(infoAboutChatUsers, "_id"),
      ]);
    });
  });
  socket.on("chat message to backend", async ({ message, id }) => {
    const Message = new MessageModel(message);
    await Message.save();
    const messages = await MessageModel.find({
      chat: message.chat,
    });
    const UsersInChat = await ChatModel.findOne({
      _id: message.chat,
    }).select({
      users: 1,
    });
    const infoAboutChatUsers = await UserModel.find({
      _id: { $in: UsersInChat.users },
    }).select({ nickname: 1, isOnline: 1, avatar: 1 });
    const newMessages = messages.map(({ _doc }) => {
      return { ..._doc };
    });
    io.to(message.chat).emit("respond all chat messages from backend", [
      newMessages,
      groupBy(infoAboutChatUsers, "_id"),
    ]);
  });
  socket.on("disconnect", async () => {
    connectionCount -= 1;
    const handshakeData = socket.request;
    console.log("user disconnected", handshakeData._query["nick"]);
    try {
      await UserModel.findOneAndUpdate(
        {
          nickname: handshakeData._query["nick"],
        },
        { isOnline: false }
      );
      next();
    } catch (error) {}
  });
});

module.exports = { http, app, express };
